/*
Author: SK Muinuddin Haider Chesty
Date: 23rd December, 2017
Youtube Channel: Learn Coding From Scratch

-------------------------------------------------------------
This class builds the User Interface of the Stop Watch app. |
-------------------------------------------------------------
 */


package stopWatch;
import javax.swing.*;
import java.awt.*;

public class UI extends JFrame {

    private GridBagLayout gridBagLayout = new GridBagLayout();
    private GridBagConstraints gridBagConstraints = new GridBagConstraints();
    private JLabel secondsLabel = new JLabel("00");
    private JLabel minutesLabel = new JLabel("00:");
    private JLabel hoursLabel = new JLabel("00:");
    private JLabel text = new JLabel("Learn Coding From Scratch");
    private Font fontTime = new Font(Font.SERIF,Font.BOLD,30);
    private Font fontButton = new Font(Font.MONOSPACED,Font.LAYOUT_LEFT_TO_RIGHT,15);
    private JButton start = new JButton("Start");
    private JButton reset = new JButton("Reset");
    private JButton exit = new JButton("X");
    private Event event = new Event(this);

    public void setSecondsLabel(String seconds)
    {
        secondsLabel.setText(seconds);
    }

    public void setMinutesLabel(String minutes)
    {
        minutesLabel.setText(minutes);
    }

    public void setHoursLabel(String hours)
    {
        hoursLabel.setText(hours);
    }

    /*
    ### Constructor of this class.
    ### I am calling the settings() "function" to construct and add all the objects into my JFrame
         */
    UI()
    {
        settings();
        events();

    }

    private void events()
    {
        reset.addActionListener((l)->{
            event.resetButtonEvent();
        });
        exit.addActionListener((l)->{
            event.exitButtonEvent();
        });
        start.addActionListener((l)->{
            event.start();
        });
    }



    /*
    The following function called "labelSettings" deals with all the JLabels in this class. This function is respoinsible
    for adding different fonts and colors to the JLabels
     */
    private void labelSettings()
    {
        secondsLabel.setFont(fontTime);
        minutesLabel.setFont(fontTime);
        hoursLabel.setFont(fontTime);
        text.setFont(new Font(Font.SANS_SERIF,Font.ITALIC,8));
        text.setForeground(new Color(109,196,106));
        text.setBackground(new Color(62,64,62));

    }



    /*
    The following function named "buttonSettings" deals with the designing part of all the JButtons in this class.
     */
    private void buttonSettings()
    {
        start.setBackground(Color.BLACK);
        start.setFont(fontButton);
        start.setForeground(Color.green);
        start.setBorder(BorderFactory.createEtchedBorder(Color.BLUE,Color.GREEN));

        reset.setBorder(BorderFactory.createLineBorder(Color.orange));
        reset.setFont(fontButton);
        reset.setBackground(Color.BLACK);
        reset.setForeground(Color.RED);

        exit.setBorder(BorderFactory.createEmptyBorder());
        exit.setFont(new Font(Font.MONOSPACED,Font.CENTER_BASELINE,20));
        exit.setBackground(Color.red);
    }



    /*
    ### First, I am calling the labelSettings() "function" in order to perform all modification I have done to each JLabels
    ### Secondly, I am calling the buttonSettings() "function" for the same reason but for the JButtons
    */
    private void settings()
    {
        labelSettings();
        buttonSettings();

        getContentPane().setBackground(Color.BLACK); // Changes the background of the whole frame to Black
        setLayout(gridBagLayout); // Setting the Layout as GridBagLayout.


        // Exit Button's positioning in the Frame
        gridBagConstraints.anchor = GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.weighty = 0;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        add(exit,gridBagConstraints);



        // Heading Label's positioning in the Frame
        gridBagConstraints.anchor = GridBagConstraints.NORTHEAST;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.weighty = 0.5;
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 0;
        add(text,gridBagConstraints);


        // Hour Label's positioning in the Frame
        gridBagConstraints.anchor = GridBagConstraints.LINE_END;
        gridBagConstraints.weighty = 1;
        gridBagConstraints.weightx = 2;
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        add(hoursLabel,gridBagConstraints);


        // Minutes Label's positioning in the Frame
        gridBagConstraints.anchor = GridBagConstraints.LINE_START;
        gridBagConstraints.weighty = 0;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        add(minutesLabel,gridBagConstraints);



        // Second Label's positioning in the Frame
        gridBagConstraints.weighty = 0;
        gridBagConstraints.weightx = 2;
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        add(secondsLabel,gridBagConstraints);


        // Start Button's positioning in the Frame
        gridBagConstraints.weighty = 0;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        add(start,gridBagConstraints);


        // Reset Button's positioning in the Frame
        gridBagConstraints.weighty = 0;
        gridBagConstraints.weightx = 0;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        add(reset,gridBagConstraints);




        /*
        Following code deals with the configuration of the frame's size, position on the screen etc.
         */
        setSize(400,150);
        setLocation(500,20);
        setUndecorated(true);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }



}
