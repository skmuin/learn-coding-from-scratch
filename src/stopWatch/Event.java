/*
AUTHOR: SK MUINUDDIN HAIDER CHESTY
DATE  : 24th December, 2017
Youtube Channel : Learn Coding From Scratch

-------------------------------------------------------
This Event class takes care of all button Events       |
-------------------------------------------------------
 */

package stopWatch;

public class Event extends Thread{

    private UI uiClass;         // UI class object allows us to access its methods.
    private int seconds = 00;   // int variable stores seconds of the watch, max value = 59
    private int minutes = 00;   // int variable stores minutes of the watch, max value = 59
    private int hours = 00;     // int variable stores hours of the watch, max value = 23
    private int resetValue = 00;// resets value to 0
    private boolean hasNotBeenReset = true; // this variable turns false when reset method is called and also terminates the Thread.

    public Event(UI ui)
    {
        this.uiClass = ui;

    }


    /*
    The following resentButtonEvent() resets all the values to initial value of 0.
         */
    public void resetButtonEvent()
    {

        minutes = resetValue;
        seconds = resetValue;
        hours = resetValue;
        uiClass.setSecondsLabel(seconds + "");
        uiClass.setMinutesLabel(minutes + ":");
        uiClass.setHoursLabel(hours + ":");
        hasNotBeenReset = false;


    }

    /*
    The following exitButtonEvent() method exits the program.
     */

    public void exitButtonEvent()
    {
      System.exit(0);
    }

    /*
    The following run() method is of Thread class. This function actually works as the clock.
     */
    public void run()
    {
        while (hasNotBeenReset) {
            try {
                Thread.sleep(1000);
                seconds++;
                uiClass.setSecondsLabel(seconds + "");         // Updating the seconds JLabel
                if (seconds > 59) {
                    minutes++;
                    uiClass.setMinutesLabel(minutes + ":");   // Updating the minutes JLabel
                    seconds = resetValue;                     // Resetting the value of seconds to 0 after reaching 59 seconds
                    uiClass.setSecondsLabel("00");            // Updating the seconds JLabel
                    if (minutes > 59) {
                        hours++;
                        uiClass.setHoursLabel(hours + ":");   // Updating the hours JLabel
                        minutes = resetValue;                 // Resetting minute after 59 minutes
                        seconds = resetValue;                 // Resetting seconds after 59 seconds
                        if (hours > 23) {
                            resetButtonEvent();               // Resets everything after 1 day

                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        resetButtonEvent(); // this function call allows us to reset everything after the Thread has been terminated.

    }




}
